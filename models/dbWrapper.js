const db = require('../config/db');
db.onConnect();
class DbWrapper {
  saveToDb(saveObject) {
    return new Promise(async (resolve, reject) => {
      try {
        const result = await saveObject.save();
        resolve(result);
      } catch (error) {
        reject(error);
      }
    });
  } 
}

module.exports = new DbWrapper();
