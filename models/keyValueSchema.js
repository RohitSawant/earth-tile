const mongoose = require('mongoose');

const { Schema } = mongoose;

const keyValueSchema = new Schema({
    key:{
        type:String,
    },
    value:{
         type: [String],
    }
});


const keyValue = module.exports = mongoose.model('Wallets', keyValueSchema);