
const CONSTANTS = require('./../config/constants');
const dbWrapper = require('../models/dbWrapper');
const keyValue =require('../models/keyValueSchema');
class RouteHandler {
  async saveKeyValues(request, response){
    try{
      const data={
        key : request.body.key,
        value : request.body.value,
      }
      const user = new keyValue(data);
      const result = await dbWrapper.saveToDb(user);
      if (result === null || result === undefined) {
        response.status(CONSTANTS.SERVER_NOT_FOUND_HTTP_CODE).json({
          error: true,
          code: CONSTANTS.SERVER_NOT_FOUND_HTTP_CODE,
          status: CONSTANTS.SERVER_NOT_FOUND_HTTP_TEXT,
          data: null,
          message: CONSTANTS.SAVING_FAILED,
        });
      } else {
        response.status(CONSTANTS.SERVER_OK_HTTP_CODE).json({
          error: false,
          code: CONSTANTS.SERVER_OK_HTTP_CODE,
          status: CONSTANTS.SERVER_OK_STATUS_TEXT,
          data: result,
          message: CONSTANTS.DATA_SAVED,
        });
      }
    } catch(e){
      console.log(e,'-----')
      response.status(CONSTANTS.SERVER_NOT_FOUND_HTTP_CODE).json({
        error: true,
        code: CONSTANTS.SERVER_INTERNAL_ERROR_HTTP_CODE,
        status: CONSTANTS.SERVER_INTERNAL_ERROR_HTTP_TEXT,
        data: null,
        message: CONSTANTS.SERVER_ERROR,
      });
    }

  }
}


module.exports = new RouteHandler();
