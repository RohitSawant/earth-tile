const fileUpload = require('express-fileupload');

const routeHandler = require('./../handlers/route-handler');


class Routes {
  constructor(app) {
    this.app = app.use(fileUpload());
  }

  appRoutes() {
    this.app.post('/saveKeyValue',routeHandler.saveKeyValues);  
  }

  routesConfig() {
    this.appRoutes();
  }
}
module.exports = Routes;
