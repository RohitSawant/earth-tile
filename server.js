const express = require('express');
const http = require('http');
const Rputes = require('./web/routes');
const AppConfig = require('./config/app-config');


class Server {
  constructor() {
    this.app = express();
    this.http = http.Server(this.app);
  }

  appConfig() {
    new AppConfig(this.app).includeConfig();
  }

  /* Including app Routes starts */
  includeRoutes() {
    new Rputes(this.app).routesConfig();
  }
  /* Including app Routes ends */

  appExecute() {
    this.appConfig();
    this.includeRoutes();
    const port = process.env.PORT || 4220;
    const host = '0.0.0.0';
    this.http.listen(port, host, () => {
      // eslint-disable-next-line no-console
      console.log(`Listening on http://${host}:${port}`);
    });
  }
}
const app = new Server();
app.appExecute();
