


module.exports.SAVING_FAILED = 'Failed top save the data';
module.exports.DATA_SAVED = 'Data is successfully saved';
module.exports.SERVER_OK_HTTP_CODE = 200;
module.exports.SERVER_BAD_REQUEST = 400;
module.exports.SERVER_REQUEST_ERROR_HTTP_CODE = 412;
module.exports.SERVER_REQUEST_ERROR_HTTP_TEXT = 'precondition failed';
module.exports.SERVER_NOT_ALLOWED_HTTP_CODE = 503;
module.exports.SERVER_OK_STATUS_TEXT = 'success';
module.exports.SERVER_NOT_FOUND_HTTP_CODE = 404;
module.exports.SERVER_NOT_FOUND_HTTP_TEXT = 'not found';
module.exports.SERVER_INTERNAL_ERROR_HTTP_CODE = 500;
module.exports.SERVER_INTERNAL_ERROR_HTTP_TEXT = 'internal server error';
module.exports.SERVER_UNAUTHORIZE_HTTP_CODE = 401;
module.exports.SERVER_UNAUTHORIZE_HTTP_TEXT = 'unauthorized';
/* HTTP status code constant ends */




