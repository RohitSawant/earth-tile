const mongoose = require('mongoose');
const dotenv = require('dotenv');

class Mongoose {
  constructor() {
    dotenv.config();
    this.mongoURL = process.env.DB_URL;
    mongoose.set('useNewUrlParser', true);
    mongoose.set('useFindAndModify', false);
    mongoose.set('useCreateIndex', true);
    mongoose.set('useUnifiedTopology', true);
  }

  async onConnect() {
    return new Promise((resolve, reject) => {
      mongoose.connect(this.mongoURL, { useNewUrlParser: true }, (err, db) => {
        if (err) {
          reject(err);
        } else {
          resolve(db);
          
        }
      });
    });
  }
}

module.exports = new Mongoose();
