
const dotenv = require('dotenv');
const bodyParser = require('body-parser');
const cors = require('cors');
const ExpressConfig = require('./express-config');

class AppConfig {
  constructor(app) {
    dotenv.config();
    this.app = app;
  }

  includeConfig() {
    this.app.use(
      bodyParser.json(),
      bodyParser.urlencoded({ extended: true }),
    );
    this.app.use(
      cors(),
    );
    new ExpressConfig(this.app);
  }
}

module.exports = AppConfig;